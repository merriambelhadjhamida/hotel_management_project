package com.example.HomeManagement.Room;

import com.example.HomeManagement.Reservation.Reservation;
import com.example.HomeManagement.RoomImage.RoomImage;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data

public class Room {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long roomId;

    private boolean availability;
    private int maxGuests;
    private double price;
    private String equipments;

    @OneToMany(mappedBy = "room",cascade = CascadeType.ALL,orphanRemoval = true)
    private List<RoomImage> roomImages=new ArrayList<>();


    @OneToMany(mappedBy = "room")
    private List<Reservation> ReservationList;



}
