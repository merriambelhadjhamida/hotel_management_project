package com.example.HomeManagement.Room;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping (path="rooms")

public class RoomController {
  private final RoomService roomService;
  @Autowired
  public RoomController(RoomService roomService){
      this.roomService=roomService;
  }

    @GetMapping("/view_dispo")
    public List<RoomDTo> get_dispo_Rooms_by_date(@RequestParam("checkInDate") LocalDate CheckInDate, @RequestParam("checkOutDate") LocalDate CheckOutDate,@RequestParam("max_guests") Double max_guests){
        return roomService.get_dispo_Rooms_by_number_of_guests(CheckInDate,CheckOutDate,max_guests);
    }

    @PostMapping("/add_room")
    public ResponseEntity<String> add_new_room(@RequestBody RoomDTo roomDTo){
      return roomService.add_new_room(roomDTo);
    }
    @PutMapping("/update_room/{room_id}")
    public ResponseEntity<String> update_room( @PathVariable("room_id") Long room_id  ,@RequestBody RoomDTo roomDTo){
     return roomService.update_room(room_id,roomDTo);
    }

    @DeleteMapping("/delete_room/{room_id}")

    public ResponseEntity<String> delete_room(@PathVariable("room_id") Long room_id){
     return roomService.delete_room(room_id);
    }
    @PutMapping("/update_availability")
    public ResponseEntity<String> update_availability(){
      return roomService.update_availability();
    }
}
