package com.example.HomeManagement.Room;

import com.example.HomeManagement.RoomImage.RoomImage;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class RoomDTo {


    private Long id;
    private double price;
    private String description;
    private List<String> photoUrls;
    private int max_guests;



    public static RoomDTo toDto(Room entity){
       return RoomDTo.builder().id(entity.getRoomId())
               .price(entity.getPrice())
               .description(entity.getEquipments())
               .max_guests(entity.getMaxGuests())
               .photoUrls(entity.getRoomImages()
               .stream()
               .map(RoomImage::getImageUrl)
               .collect(Collectors.toList()))
               .build();
   }
    public static Room toEntity(RoomDTo roomDTo) {
       Room room= Room.builder()
                .price(roomDTo.getPrice())
                .equipments(roomDTo.getDescription())
                .maxGuests(roomDTo.getMax_guests())
                .build();
       if(roomDTo.getPhotoUrls()!=null && !roomDTo.getPhotoUrls().isEmpty()){
           List<RoomImage> roomImages = new ArrayList<>();
           for (String s : roomDTo.getPhotoUrls()) {
               RoomImage roomImage = new RoomImage();
               roomImage.setImageUrl(s);
               roomImage.setRoom(room);
               roomImages.add(roomImage);
           }
           room.setRoomImages(roomImages);}
        return room;

    }



}
