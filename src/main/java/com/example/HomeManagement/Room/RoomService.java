package com.example.HomeManagement.Room;

import com.example.HomeManagement.Reservation.Reservation;
import com.example.HomeManagement.Reservation.ReservationService;
import com.example.HomeManagement.RoomImage.RoomImage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RoomService {

    private final RoomRespository roomRespository;
    private final ReservationService reservationService;


    public  ResponseEntity<String> add_new_room(RoomDTo roomDTo) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("Admin"))) {
            Room room=RoomDTo.toEntity(roomDTo);
            room.setAvailability(true);
            roomRespository.save(room);
            return ResponseEntity.ok("welcome you are authorized as an admin ");
             }
        else{
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You are not authorized as an admin.");

        }

    }


    public List<RoomDTo> get_dispo_Rooms_by_number_of_guests(LocalDate CheckInDate, LocalDate CheckOutDate,Double max_guests) {
        List<Room> roomList = roomRespository.findAll();
        List<RoomDTo> roomDetails_available = new ArrayList<>();

        for (Room room : roomList) {
            Long id= room.getRoomId();
            boolean isRoomAvailable =reservationService.IsRoomAvailable(id,CheckInDate,CheckOutDate);
            if (isRoomAvailable && room.getMaxGuests() >= max_guests)            {
                    RoomDTo roomDetails =RoomDTo.toDto(room);
                    roomDetails_available.add(roomDetails);
                }

            }





        return  roomDetails_available;

    }

    public ResponseEntity<String> update_room(Long room_id,RoomDTo roomDTo) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("Admin"))) {

        Room room=roomRespository.findById(room_id).orElseThrow(()-> new IllegalStateException("this room id is not found"));
        if(roomDTo.getDescription()!=null){
            room.setEquipments(roomDTo.getDescription());
        }
        if(roomDTo.getMax_guests()!=0){
            room.setMaxGuests(roomDTo.getMax_guests());
        }
        if(roomDTo.getPrice()!=0){
            room.setPrice(roomDTo.getPrice());
        }
        if(roomDTo.getPhotoUrls()!=null){

            List<RoomImage> roomImages=roomDTo.getPhotoUrls().stream().map(url->{
                RoomImage roomImage=new RoomImage();
                roomImage.setImageUrl(url);
                return roomImage;
            }).collect(Collectors.toList());
            room.setRoomImages(roomImages);


        }

        roomRespository.save(room);
        return ResponseEntity.ok("welcome you are authorized as an admin ");

        }
        else{
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You are not authorized as an admin.");

        }
    }

    public ResponseEntity<String> delete_room(Long roomId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("Admin"))){

        Room room = roomRespository.findById(roomId).orElseThrow(() -> new IllegalStateException("this room id is not found"));
        List<Reservation> reservationList=room.getReservationList();
        for(Reservation reservation:reservationList) {

            reservation.setRoom(null);

        }
        roomRespository.deleteById(roomId);
        return ResponseEntity.ok("welcome you are authorized as an admin ");



        }
        else{
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You are not authorized as an admin");

        }
    }


    public ResponseEntity<String> update_availability() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("Admin"))){

       List<Room>  roomList = roomRespository.findAll();

       for(Room room:roomList) {
           boolean isAvailable=true;

           if (room.getReservationList() != null) {
               List<Reservation> reservationList = room.getReservationList();
               for (Reservation reservation : reservationList) {
                   if ("Confirmed".equals(reservation.getStatus())) {
                       LocalDate checkInDate=reservation.getCheckInDate();
                       LocalDate checkOutDate=reservation.getCheckOutDate();
                       LocalDate currentDate=LocalDate.now();
                       if(!currentDate.isBefore(checkInDate) && !currentDate.isAfter(checkOutDate)){
                           isAvailable=false;
                           break;
                       }

                   }
               }

           }

           room.setAvailability(isAvailable);

           roomRespository.save(room);
       }
       return ResponseEntity.ok("welcome you are authorized as an admin ");
        }
    else{
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You are not authorized as an admin");

    }
}}
