package com.example.HomeManagement.RoomImage;

import com.example.HomeManagement.Room.Room;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Table(name="roomImage")
public class RoomImage {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String imageUrl;
    @ManyToOne
    @JoinColumn(name = "roomId")
    private Room room;







}
