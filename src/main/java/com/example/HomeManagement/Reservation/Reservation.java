package com.example.HomeManagement.Reservation;

import com.example.HomeManagement.Room.Room;

import com.example.HomeManagement.User.User;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ReservationId;
    private LocalDate checkInDate;
    private LocalDate checkOutDate;
    @ManyToOne
    private Room room;
    private String status;
    @ManyToOne
    private User user;

}
