package com.example.HomeManagement.Reservation;

import com.example.HomeManagement.Config_security.JwtService;
import com.example.HomeManagement.Room.Room;
import com.example.HomeManagement.Room.RoomDTo;
import com.example.HomeManagement.Room.RoomRespository;
import com.example.HomeManagement.RoomImage.RoomImage;
import com.example.HomeManagement.User.UserRepository;
import com.example.HomeManagement.User.User;
import lombok.RequiredArgsConstructor;
import org.hibernate.grammars.hql.HqlParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ReservationService {
    private  final ReservationRepository reservationRepository;
    private final RoomRespository roomRespository;
    private final UserRepository userRepository;
    private final JwtService jwtService;




    public ResponseEntity<String> registerNewReservation(Long roomId, LocalDate checkInDate, LocalDate checkOutDate, String token) {
        LocalDate currentDate = LocalDate.now();
        if (checkInDate.isAfter(currentDate) || checkInDate.isEqual(currentDate)) {
            if (checkInDate.isBefore(checkOutDate)) {

                if (IsRoomAvailable(roomId, checkInDate, checkOutDate)) {
                    Reservation reservation = new Reservation();
                    reservation.setCheckInDate(checkInDate);
                    reservation.setCheckOutDate(checkOutDate);
                    Room room = roomRespository.findById(roomId).orElseThrow(() -> new IllegalStateException("room id not found"));
                    String jwt = token.substring(7);
                    String username = jwtService.extractUsername(jwt);
                    User guest = userRepository.findByEmail(username).orElseThrow(() -> new IllegalStateException("this username is not found"));
                    reservation.setRoom(room);
                    reservation.setCheckOutDate(checkOutDate);
                    reservation.setCheckInDate(checkInDate);
                    reservation.setUser(guest);
                    reservation.setStatus("Confirmed");
                    reservationRepository.save(reservation);
                    return ResponseEntity.ok("reservation registered with success");

                } else {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("This room is not available for those dates");
                }
            }
            else{
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(" Warning your  checkindate is after the checkoutdate ");
            }
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Warning your checkindate must be on or after the current date  ");
        }
    }




            public List<ReservationDTo> voir_historique_reservations (String token){

                String jwt = token.substring(7);
                String username = jwtService.extractUsername(jwt);
                User guest = userRepository.findByEmail(username).orElseThrow(() -> new IllegalStateException("this username is not found"));

                List<Reservation> reservationList = guest.getReservationList();
                List<ReservationDTo> reservationDTos = new ArrayList<>();
                for (Reservation reservation : reservationList) {

                    ReservationDTo reservationDetails = ReservationDTo.ToDto(reservation);
                    reservationDTos.add(reservationDetails);

                }

                return reservationDTos;

            }


            public ResponseEntity<String> update_reservation (Long reservationId, String token, LocalDate
            checkOutDate, LocalDate checkInDate){

                String jwt = token.substring(7);
                String username = jwtService.extractUsername(jwt);
                User guest = userRepository.findByEmail(username).orElseThrow(() -> new IllegalStateException("this username is not found"));

                List<Reservation> reservationList = guest.getReservationList();
                Reservation reservation = reservationRepository.findById(reservationId).orElseThrow(() -> new IllegalStateException("this reservation id is not found"));
                if (reservationList.contains(reservation)) {
                    if(reservation.getStatus().equals("Completed")){
                    Room room = reservation.getRoom();
                    Long room_id = room.getRoomId();
                    if (IsRoomAvailable(room_id, checkInDate, checkOutDate)) {
                        reservation.setRoom(room);
                        reservation.setCheckInDate(checkInDate);
                        reservation.setCheckOutDate(checkOutDate);
                        reservationRepository.save(reservation);
                        return ResponseEntity.ok("update was successfull");

                    } else {
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("this room is not available for these dates");
                    }  }
                    else {
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("this reservation is completed")  ;

                    }

                } else {
                    return ResponseEntity.status(HttpStatus.FORBIDDEN).body("you are not authorized to update this reservation");
                }


            }

            public ResponseEntity<List<ReservationDTo>> view_all_reservations () {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (authentication != null && authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("Admin"))) {
                    List<Reservation> reservationList = reservationRepository.findAll();
                    List<ReservationDTo> reservationDTos = new ArrayList<>();
                    for (Reservation reservation : reservationList) {
                        ReservationDTo reservationDetails = ReservationDTo.ToDto(reservation);
                        reservationDTos.add(reservationDetails);
                    }
                    return ResponseEntity.ok(reservationDTos);


                } else {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(Collections.emptyList());
                }

            }

            public ResponseEntity<String> update_status () {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (authentication != null && authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("Admin"))) {

                    List<Reservation> reservationList = reservationRepository.findAll();
                    for (Reservation reservation : reservationList) {
                        Room room = reservation.getRoom();
                        LocalDate currentDate = LocalDate.now();

                        if (currentDate.isAfter(reservation.getCheckOutDate())) {
                            reservation.setStatus("Completed");
                            reservationRepository.save(reservation);

                        } else if (room == null && !("Completed").equals(reservation.getStatus())) {
                            reservation.setStatus("Canceled");
                        }


                        reservationRepository.save(reservation);
                    }
                    return ResponseEntity.ok("welcome you are authorized as an admin ");

                } else {
                    return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You are not authorized as an admin.");

                }

            }

            public boolean IsRoomAvailable (Long roomId, LocalDate checkInDate, LocalDate checkOutDate){
                Room room = roomRespository.findById(roomId).orElseThrow(() -> new IllegalStateException("room id not found"));
                boolean isRoomAvailable = true;

                List<Reservation> reservationList = room.getReservationList();
                if (reservationList != null) {

                    for (Reservation reservation : reservationList) {
                        LocalDate reservationArrivalDate = reservation.getCheckInDate();
                        LocalDate reservationDepartureDate = reservation.getCheckOutDate();
                        if ("Confirmed".equals(reservation.getStatus())) {
                            if (checkInDate.isBefore(reservationDepartureDate) && checkOutDate.isAfter(reservationArrivalDate)) {
                                isRoomAvailable = false;
                                break;
                            }
                        }

                    }
                }
                return isRoomAvailable;
            }
        }
