package com.example.HomeManagement.Reservation;

import com.example.HomeManagement.Room.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("reservation")
public class ReservationController {
    private  final ReservationService reservationService;
    @Autowired
    public ReservationController(ReservationService reservationService){
        this.reservationService=reservationService;
    }
    @PostMapping("{room_id}")
    public ResponseEntity<String> registerNewReservation(@PathVariable("room_id") Long roomId,@RequestParam("checkInDate") LocalDate checkInDate,@RequestParam("checkOutDate") LocalDate checkOutDate,@RequestHeader("Authorization") String token){
       return reservationService.registerNewReservation(roomId,checkInDate,checkOutDate,token);
    }

    @GetMapping("/historique_reservation")
    public List <ReservationDTo> voir_historique_reservations(@RequestHeader("Authorization") String token){
      return   reservationService.voir_historique_reservations(token);
    }

    @PutMapping(path="update/{reservation_id}")
    public  ResponseEntity<String> update_reservation(@PathVariable ("reservation_id") Long reservation_id,@RequestHeader("Authorization") String token,@RequestParam("checkoutdate") LocalDate CheckOutDate,@RequestParam("checkindate") LocalDate CheckInDate){

        return reservationService.update_reservation(reservation_id,token,CheckOutDate,CheckInDate);

    }
    @GetMapping("/view-all")
    public ResponseEntity<List<ReservationDTo>>  view_all_reservations(){
        return reservationService.view_all_reservations();
    }
    @PutMapping("/update_status")
    public ResponseEntity<String> update_status(){
        return reservationService.update_status();
    }



}
