package com.example.HomeManagement.Reservation;

import com.example.HomeManagement.Room.Room;
import com.example.HomeManagement.Room.RoomDTo;
import com.example.HomeManagement.User.UserDTo;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReservationDTo {
    private Long ReservationId;
    private LocalDate checkInDate;
    private LocalDate checkOutDate;
    private RoomDTo room;
    private UserDTo user;
    private String status;
    public static  ReservationDTo ToDto(Reservation entity){
        return ReservationDTo
                .builder()
                .ReservationId(entity.getReservationId())
                .checkInDate(entity.getCheckInDate())
                .checkOutDate(entity.getCheckOutDate())
                .room(RoomDTo.toDto(entity.getRoom()))
                .user(UserDTo.ToDto(entity.getUser()))
                .status(entity.getStatus())
                .build();
    }





}
