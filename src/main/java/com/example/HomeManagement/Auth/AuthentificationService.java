package com.example.HomeManagement.Auth;

import com.example.HomeManagement.Config_security.JwtService;
import com.example.HomeManagement.User.Role;
import com.example.HomeManagement.User.User;
import com.example.HomeManagement.User.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static com.example.HomeManagement.User.Role.Admin;
import static com.example.HomeManagement.User.Role.Guest;

@Service
@RequiredArgsConstructor
public class AuthentificationService {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    public AuthentificationResponse register(RegisterRequest request) {

        User user = User.builder().firstname(request.getFirstname()).lastname(request.getLastname()).email(request.getEmail()).password(passwordEncoder.encode(request.getPassword())).role(Guest).build();

        repository.save(user);
        String jwtToken= jwtService.generateToken(user);
        return AuthentificationResponse.builder().token(jwtToken).build();

    }

    public AuthentificationResponse authenticate(AuthentificationRequest request) {
        var auth= authenticationManager.authenticate(

                new UsernamePasswordAuthenticationToken(request.getEmail(),request.getPassword())

        );
        User user =  repository.findByEmail(request.getEmail()).orElseThrow(()-> new UsernameNotFoundException("this username is not found"));
        String jwtToken= jwtService.generateToken(user);
        return AuthentificationResponse.builder().token(jwtToken).build();
    }

}

