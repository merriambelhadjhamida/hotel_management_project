package com.example.HomeManagement.Auth;

import lombok.*;

@Data
@Builder
@AllArgsConstructor

public class AuthentificationRequest {

    private String email;
    private String password;
}
