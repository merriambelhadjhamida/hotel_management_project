package com.example.HomeManagement.User;

import com.example.HomeManagement.Auth.RegisterRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository repository;
    private final UserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;

    public ResponseEntity<String> add_admin(RegisterRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("Admin"))) {

            User user = User.builder().firstname(request.getFirstname()).lastname(request.getLastname()).email(request.getEmail()).password(passwordEncoder.encode(request.getPassword())).role(Role.Admin).build();

            repository.save(user);

            return ResponseEntity.ok("admin registered successfully");


        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("not authorized to register an admin");
        }
    }

    public ResponseEntity<List<UserDTo>> view_users() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("Admin"))) {
            List<User> userList=repository.findAll();
           List<UserDTo> userDTos= userList.stream().map(UserDTo::ToDto).toList();
            return ResponseEntity.ok(userDTos);

        }
        else {
            return  ResponseEntity.status(HttpStatus.FORBIDDEN).body(Collections.emptyList());
        }

    }
}

