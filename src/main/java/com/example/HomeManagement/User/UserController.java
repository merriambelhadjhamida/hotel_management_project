package com.example.HomeManagement.User;

import com.example.HomeManagement.Auth.AuthentificationResponse;
import com.example.HomeManagement.Auth.RegisterRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("admin")

public class UserController {


    private final UserService userService;
    @PostMapping ("/add_admin")
    public ResponseEntity<String> add_admin(@RequestBody RegisterRequest request){
        return userService.add_admin(request);
    }
    @GetMapping( "/view_users")
        public ResponseEntity<List<UserDTo>> view_users(){
        return userService.view_users();
        }

}
