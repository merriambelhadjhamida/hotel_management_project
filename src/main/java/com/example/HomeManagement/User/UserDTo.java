package com.example.HomeManagement.User;

import com.example.HomeManagement.Reservation.Reservation;
import com.example.HomeManagement.Reservation.ReservationDTo;
import com.example.HomeManagement.Room.RoomDTo;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTo {
    private Long userId;
    private String firstname;
    private String lastname;
    private Role role;
    public static UserDTo ToDto(User entity){
        return UserDTo
                .builder()
                .userId(entity.getUserId())
                .firstname(entity.getFirstname())
                .lastname(entity.getLastname())
                .role(entity.getRole())
                .build();

    }


}
